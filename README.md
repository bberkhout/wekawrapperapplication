# Java command line application

The purpose of this application is predicting the bird group based on bird bone dimension.
The application takes a log2 transformed CSV file and predicts which bird group each bird belongs to.
It also takes a single instance to predict.

## Installation

This application uses Java 11.0.4, make sure you have installed Java 11.0.4 or higher in order to run this program.

## Usage:

The application must first be cloned to your own workspace in order to be executed:

Open a terminal and navigate to the folder you want the application in.

    cd <your folder>
    
Clone this repository by copy and pasting the link on top of the page in your command line.

    git clone <Link above>
    
Navigate to the libs folder

    cd build/libs


When you are in the correct folder, you can see a .Jar file, execute this through the command line with the following statements based on your goal:

### To input a log2 transformed datafile:

    java -jar WekaWrapper-Jar-1.1-SNAPSHOT.jar -f <Path to your datafile>
    
### To input a log2 transformed single instance, the single instance must be of this format:

    huml,humw,ulnal,ulnaw,feml,femw,tibl,tibw,tarl,tarw
    
Command:

    java -jar WekaWrapper-Jar-1.1-SNAPSHOT.jar -l <Single instance>

### To get help on how to run the application, type the following:

    java -jar WekaWrapper-Jar-1.1-SNAPSHOT,jar -h


