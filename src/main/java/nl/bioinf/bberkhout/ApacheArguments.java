package nl.bioinf.bberkhout;

import com.opencsv.CSVWriter;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.DefaultParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Option;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;

import java.io.*;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;

/**
 * Class to get the options from the command line and parse these.
 *
 * @author Benjamin Berkhout
 */
public class ApacheArguments {
    private static final String FILE = "file";
    public static final String INSTANCE = "inst";
    private static final String HELP = "help";

    private final String[] clArguments;
    private Options options;
    private CommandLine commandLine;
    private String input;

    public ApacheArguments(final String[] args) {
        this.clArguments = args;
        initialize();
    }

    /*
    Options initialization and processing.
     */
    private void initialize() {
        buildOptions();
        processCommandLine();
    }
    /*
    creates and adds options to be used
     */
    private void buildOptions() {
        this.options = new Options();
        Option inputFile = new Option("f", FILE, true, "input .csv file for prediction " +
                "data must be log2 transformed");
        Option SingleAttr = new Option("l", INSTANCE, true, "Single line of instance, must be log2 transformed " +
                "And must be this format: huml,humw,ulnal,ulnaw,feml,femw,tibl,tibw,tarl,tarw");
        Option Help = new Option("h", HELP, false, "Prints this help message");

        options.addOption(inputFile);
        options.addOption(SingleAttr);
        options.addOption(Help);
    }

    /*
    Fetches the input given directly from the commandline
     */
    private String processCommandLine() {
        try {
            // Create a new Weka IBk runner
            WekaRunner wekaIBk = new WekaRunner();
            // Parse the command line input
            CommandLineParser cmdParser = new DefaultParser();
            this.commandLine = cmdParser.parse(this.options, this.clArguments);

            // Checks if a file has been given and if this file has the right type
            if (commandLine.hasOption(FILE)) {
                input = commandLine.getOptionValue(FILE);
                if(extensionCheck(input)){
                    wekaIBk.start(input);
                }
            }
            // Fetched single given instance from the command line
            else if (commandLine.hasOption(INSTANCE)) {
                input = commandLine.getOptionValue(INSTANCE);
                //Write the instance to a csv file in order for it to be used
                String singleInstanceDatafile = writeToFile(input);
                wekaIBk.start(singleInstanceDatafile);
            }
            // Prints the application help
            else if (commandLine.hasOption(HELP)) {printHelp();}
            else {
                printHelp();
            }
        } catch (ParseException ex) {
            throw new IllegalArgumentException(ex);
        }
        return input;
    }

    /*
    Method to check te file extension type,
    returns the boolean isValidExtension.
     */
    private boolean extensionCheck(String datafile) {
        String fileExtension;
        File file = new File(datafile);
        fileExtension = getFileExtension(file);
        boolean isValidExtension = false;
        // Checks if fileExtension is a .csv type
        if (fileExtension.equals(".csv")) {
            isValidExtension = true;
        }
        else{
            System.err.println("Please input a .csv file");
            }
        return isValidExtension;
        }

    /*
    Fetches the file extension of the given file
     */
    private String getFileExtension(File file) {
        String name = file.getName();
        int lastIndexOf = name.lastIndexOf(".");
        if (lastIndexOf == -1) {
            return ""; // empty extension
        }
        return name.substring(lastIndexOf);
    }

    /*
    Writes the given single instance to a new .csv file
    in order for it to be used in the WekaRunner
     */
    private static String writeToFile(String args){
        String fileName = "../../data/singleInstance.csv";

        try{
            CSVWriter writer = new CSVWriter(new FileWriter(fileName));

            // Create the attributes
            String[] attr = {"huml", "humw", "ulnal", "ulnaw", "feml", "femw",
            "tibl", "tibw", "tarl", "tarw"};

            String[] instance = args.split(",");

            // Adding the new instances and attributes to the file
            List<String[]> list = new ArrayList<>();
            list.add(attr);
            list.add(instance);

            writer.writeAll(list);
            writer.flush();
        }
        catch (IOException e){
            System.err.println("Data could not be parsed");
        }

        return fileName;
    }

    /*
    Print the application help.
     */
    public void printHelp() {
        HelpFormatter formatter = new HelpFormatter();
        formatter.printHelp("Weka App", options);
    }
}





