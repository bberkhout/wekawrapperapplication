package nl.bioinf.bberkhout;


import weka.classifiers.lazy.IBk;
import weka.classifiers.meta.Vote;
import weka.core.Instance;
import weka.core.Instances;
import weka.core.converters.CSVLoader;
import weka.filters.Filter;
import weka.filters.unsupervised.attribute.Add;

import java.io.File;

/**
 * Class to use a predefined IBk model and predict the labels for the given instances
 *
 * @author Benjamin Berkhout
 */
public class WekaRunner {

    public void start(String unknownFile) {

        try {
            // load pre-made IBk model
            IBk loadedModel = loadClassifier();

            CSVLoader csvLoader = new CSVLoader();
            csvLoader.setSource(new File(unknownFile));

            // Create the attributes that will be used
            csvLoader.setNominalAttributes("first-last");
            Instances unknownInstancesFromCsv = csvLoader.getDataSet();

            if (unknownInstancesFromCsv.classIndex() == -1)
                unknownInstancesFromCsv.setClassIndex(unknownInstancesFromCsv.numAttributes() - 1);

            Instances newData = new Instances(unknownInstancesFromCsv);

            // add the class attributes that will be used for prediction
            Add filter = new Add();
            filter.setAttributeIndex("last");
            filter.setNominalLabels("P,R,T,SW,W,SO");
            filter.setAttributeName("type");
            filter.setInputFormat(newData);
            newData = Filter.useFilter(newData, filter);

            newData.setClassIndex(newData.numAttributes() - 1);
            System.out.println(newData.numInstances());
            System.out.println("newData # attributes = " + newData.numAttributes());
            System.out.println("newData class index= " + newData.classIndex());

            System.out.println("unknownInstancesFromCsv = " + newData);

            // classifying new instances
            classifyNewInstance(loadedModel, newData);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    Classify the new instances using the IBk model
     */
    private void classifyNewInstance(IBk nearestNeighbour, Instances unknownInstances) throws Exception {
        Instances labeled = new Instances(unknownInstances);

        // label the new instances
        for (int i = 0; i < unknownInstances.numInstances(); i++) {
            Instance instance = unknownInstances.instance(i);
            double clsLabel = nearestNeighbour.classifyInstance(instance);
            labeled.instance(i).setClassValue(clsLabel);
        }
        System.out.println("\nNew, labeled = \n" + labeled);
    }

    /*
    Load the pre-made IBk classifier
     */
    private IBk loadClassifier() throws Exception {
        // deserialize model
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File("../../data/IBk_rec.model");
        return (IBk) weka.core.SerializationHelper.read(String.valueOf(file));
    }
}